//export const API_URL = process.env.REACT_APP_API_URL;
export const API_URL = 'https://cmpro.sentuhdigital.id';
//export const API_URL = 'https://cms.sentuhdigital.com';
export const AUTHENTICATION_KEY = "authentication";
export const CHUNK_SIZE = 2 * 1000 * 1000; // 2 MB
