import axios from "axios";
//import { useStore } from "~stores";
import { API_URL } from "./constants";

axios.interceptors.request.use(
  (config) => {
    config.baseURL = API_URL;
    if (!config.headers.Authorization) {
      //const token = useStore.getState().authStore.token;
      const token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImM0YjRlZDdmZmJiMWE4ZDgxOWI4ZmI5MTg1OGYxNWExMzRjNjJiMzY4MGIwYTI3OWNlZThhMDVkZWM5YzA1YWQyMjVhOWNkZmU5NThlMGJlIn0.eyJhdWQiOiIzIiwianRpIjoiYzRiNGVkN2ZmYmIxYThkODE5YjhmYjkxODU4ZjE1YTEzNGM2MmIzNjgwYjBhMjc5Y2VlOGEwNWRlYzljMDVhZDIyNWE5Y2RmZTk1OGUwYmUiLCJpYXQiOjE2NDg5MzYyNTgsIm5iZiI6MTY0ODkzNjI1OCwiZXhwIjoxNjgwNDcyMjU4LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.cLq1K1cMBD3KXn5HpatDx17maKQ9iiVOncWtQ_9FF5EeqeA1krJZQhW7s7SaTjK4dT3uO7IGZf6JIGEL3rTf7-AgX2svUdpkp64JMLd_seK1cuSnjHBQ1CP6hLfqsEVePpWKiGrP5dg_VKSoPJeU3Xz2mGG088DS7VSBDzcjVZpRMSVkX3wqA_UXp7dTvu9mkQHV43wUW6PJxP2mOH6w-Lt5r1Gj_O4U1mzP0uMXd77NZZzzfrch_JfoTXTgsEF1Xbf2rjHjv4az5LJRgB_VkzUM6WlIDG1xEkGvPRDfpTLR4sVAHyx_OnTPgo_VNZdsXZ-Vsj_x_TI1TUn6XxBumYzjAzOn-3c909005V5gf-eZFXkHC1MT078aHwVb9UEpVbxr3R7XvjJ8tMSEsQx7BoLu-yvl2V1jHVHvqWFuVqLk2m683pOhGl0vFK28D6Zj18CL09iJWICtrju-cFphyePZQkuSH1EjnGDri5sGMotXRRUM3349uMHo7N4p8b6z4g4ZclrCBBTELB78WbP920h49pS9oJduEFa0p-yfNZjflaft9MrRhR8c4MKWcWnb3eMwGWPMOz-m4A5ynoYX8EFD9r7x0jEQ9g5dcQlZVirhekgGK-dSBKYth4OUBg_6eQkAH1mPubyOV9pgEPtXbzsk94_zOfd1MTUQGLmBtDY";
      if (token) {
        config.headers.Authorization = `Bearer ${token}`;
      }
    }

    return config;
  },
  (error) => Promise.reject(error)
);
