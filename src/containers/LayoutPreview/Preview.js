import { useEffect, useState } from "react";
import { API_URL } from "../../config/constants";
import '../LayoutPreview/css/Layout.css';
import Frames from "./components/Frame";
import useSWR from "swr";
import { fetcherPriview } from "../../api/fetchers";
import AppLoader from "../App/component/AppLoader";

const Preview = () => {
  //CONTOH ID LAYOUT
  const id_layout = "1295";
  //const id_layout = "55";
  const [frame, setFrame] = useState([]);
  const [latar, setLatar] = useState();
  let priviewData = useSWR([`${API_URL}/api/layouts/${id_layout}/preview`], fetcherPriview);
  
  useEffect(() => {
    setFrame(priviewData.data?.frames);
    setLatar(priviewData?.data?.background_image);  
  }, [priviewData]);

  return (
    <>
      <div 
        className="container" 
        style={{ 
          backgroundImage: `url("${API_URL}${latar}")`, 
          backgroundSize:'cover'
        }}
      >
        { frame ? frame.map((frm, index) => <Frames data={frm} key={index}/>) : <AppLoader /> }
      </div>
    </>
  );
}

export default Preview;