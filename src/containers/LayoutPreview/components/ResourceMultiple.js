import Slider from 'react-animated-slider';
import 'react-animated-slider/build/horizontal.css';
import ObjImage from "./ObjImage";
import ObjVideo from "./ObjVideo";
//import ObjText from "./ObjText";
//import ObjIframe from "./ObjIframe";
//import ObjClock from "./ObjClock";

const ResourceMultiple = (props) => {
  const { data } = props;
  let frame_resources = data.frame_resources;  
  return (
    <>
      <Slider autoplay={5000} touchDisabled={false}>
        {frame_resources.map((slide, index) => <div key={index}>
          { (slide.type == 'image') ? <ObjImage data={slide.web_resource}/> : "" }
          { (slide.type == 'video') ? <ObjVideo data={slide.web_resource}/> : "" }
        </div>)}
      </Slider>
    </>    
  )  
};

export default ResourceMultiple;
