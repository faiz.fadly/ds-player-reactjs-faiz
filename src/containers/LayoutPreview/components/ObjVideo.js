import react,{useState} from 'react';
import { API_URL } from "../../../config/constants";
import ModalVideo from 'react-modal-video';

const ObjVideo = (props) => {
    const { data } = props;
    const videoSrc = `${API_URL}${data}`;
    const [isOpen, setOpen] = useState(false);

    return (
        <>
            <ModalVideo channel='custom' isOpen={isOpen} url={videoSrc} onClose={() => setOpen(false)} />
            <video className='resourceContainer' onClick={()=>setOpen(true)}
                style={{
                    width:'100%',  
                    height: 'auto', 
                    borderRadius:8, 
                    marginTop:5
                }}
                src={videoSrc} 
                autoPlay 
                loop 
                muted>    
            </video>
        </>
    );
};
export default ObjVideo;
