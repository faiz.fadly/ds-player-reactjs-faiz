import ReactWeather, { useOpenWeather } from 'react-open-weather';
import '../css/Layout.css';

const ObjWeather = (props) => {
    const { data, isLoading, errorMessage } = useOpenWeather({
        key: '65bb399f65b9d488eb1c7a85885447d0',
        lat: '-6.2146',
        lon: '106.8451',
        lang: 'en',
        unit: 'imperial', // values are (metric, standard, imperial)
    });


    return (
        <div className="resourceContainer">
            <ReactWeather
                isLoading={isLoading}
                errorMessage={errorMessage}
                data={data}
                lang="en"
                locationLabel="Indonesia"
                unitsLabels={{ temperature: 'C', windSpeed: 'Km/h' }}
                showForecast
            />
        </div>
    );
};
export default ObjWeather;
