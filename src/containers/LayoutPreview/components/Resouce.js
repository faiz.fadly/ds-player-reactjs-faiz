import ObjImage from "./ObjImage";
import ObjText from "./ObjText";
import ObjVideo from "./ObjVideo";
import ObjIframe from "./ObjIframe";
import ObjClock from "./ObjClock";
import ObjWeather from "./ObjWeather";

const Resource = (props) => {
  const { data } = props;
  let type = data[0]?.type;
  return (
    <>   
        { (type == 'text') ? <ObjText data={data[0]} /> : "" } 
        { (type == 'image') ? <ObjImage data={data[0]?.web_resource} /> : "" }
        { (type == 'video') ? <ObjVideo data={data[0]?.web_resource} /> : "" } 
        { (type == 'clock') ? <ObjClock data={data[0]?.web_resource} /> : "" } 
        { (type == 'html') ? <ObjIframe data={data[0]?.web_resource} type='web'/> : "" } 
        { (type == 'powerpoint') ? <ObjIframe data={data[0]} type='powerpoint'/> : "" } 
    </>
  )  
};

export default Resource;
