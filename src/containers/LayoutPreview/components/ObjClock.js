import dayjs from "dayjs";
import { useEffect, useState } from "react";
import '../css/digitalFont.css'

const getClock = () => {
  //let utc = new Date().getTimezoneOffset() / -60;
  //utc = `(GMT ${utc > 0 ? "+" : "-"}${utc})`;
  const time = dayjs().format("HH:MM:ss");

  return `${time}`;
};

const Clock = () => {
  const [clock, setClock] = useState(getClock());

  const clockContainer = {
    transform: `translateY(33%)`,
    textAlign: 'center',
    borderRadius: 5,
    with: '100%',
    height:'auto',
    backgroundColor:`rgba(0, 0, 0, 0.5)`
  };

  const timeStyle = {
    fontFamily: 'Orbitron',
    fontSize: '1.5em',
    textAlign: 'center',
    color: '#fff',
   };

  useEffect(() => {
    const intervalId = setInterval(() => {
      setClock(getClock());
    }, 100);

    return () => {
      clearInterval(intervalId);
    };
  }, []);

  return (
    <div style={clockContainer}>
      <span style={timeStyle}>{clock}</span>
    </div>
  );
};

const ObjClock = () => {
  return (
    <Clock />  
  );
};

export default ObjClock;
