const ObjText = (props) => {
    const { data } = props;
    const resource = data?.web_resource;
    
    return (
        <div style={{
            // backgroundColor : `${resource.background-color}`,
            // color : `${resource.text-color}`,
            backgroundColor : `#ef5a23`,
            color : `#fff`,
            fontSize:'1em',
            paddingTop : 12,
            paddingBottom : 12,
            borderRadius:10,
        }}>
            <marquee crolldelay="500">
                {resource.text}
            </marquee>
        </div>
    );
}
export default ObjText;
