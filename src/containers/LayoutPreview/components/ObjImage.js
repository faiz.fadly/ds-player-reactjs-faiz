import { API_URL } from "../../../config/constants";
import '../css/Layout.css';

import ModalImage from "react-modal-image";

const ObjImage = (props) => {
    const { data } = props;
    let img_url = `${API_URL}${data}`;
    return (
        <div className="resourceContainer">
            <ModalImage
                small={img_url}
                large={img_url}
                alt="Image"
                style={{
                    width:'100%', 
                    borderRadius:8
                }}
                className='resourceContainer'
            />
        </div>
    );
};
export default ObjImage;
