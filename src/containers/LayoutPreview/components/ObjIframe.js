import { API_URL } from "../../../config/constants";
const ObjIframe = (props) => {
    const { data, type } = props;
    const resource = data?.web_resource;
    const dependency = 'https://view.officeapps.live.com/op/embed.aspx?src=';
    let url="";
    if(type=='powerpoint'){
        url = `${dependency}${API_URL}${resource}`;
    }else{
        url = resource?.url == 'undefined' ? 'https://ibank.bni.co.id/' : resource?.url;
    }
    
    return (
        <iframe 
            src={url} 
            style={{
                width:'100%', 
                height:'100%', 
                border:0, 
                borderRadius:8
            }}>
        </iframe>
    );
};
export default ObjIframe;
