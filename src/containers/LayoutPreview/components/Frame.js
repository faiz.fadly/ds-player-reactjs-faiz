import Resouce from "./Resouce";
import ResourceMultiple from "./ResourceMultiple";

const Frames = (props) => {
  const { data } = props;
  let lebar = (parseInt(data.width)/1.3); //(parseInt(data.width)/2.6);
  let tinggi = (parseInt(data.height)/1.3); //(parseInt(data.height)/2.6);
  let x = parseInt(data.x)/1.3; //parseInt(data.x)/2.6;
  let y = parseInt(data.y)/1.3; //parseInt(data.y)/2.6;           
  let frame_resources= data.frame_resources;
  let count = Object.keys(frame_resources).length;

  return (
    <div style={{borderRadius:13, 
        position:'absolute', 
        width:parseInt(lebar), 
        height:parseInt(tinggi),
        right:parseInt(x), 
        top: parseInt(y),
        marginRight: 19,
        //backgroundColor: 'blanchedalmond',
      }}>
        { frame_resources ? 
          (count > 1 ? <ResourceMultiple data={data}/> : <Resouce data={frame_resources} />) : "This Frame Have No Resource" 
        } 
    </div>
   );
};

export default Frames;
