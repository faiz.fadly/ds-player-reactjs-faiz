import { ChakraProvider } from "@chakra-ui/react";
// import produce from "immer";
// import { useEffect } from "react";
// import theme from "~config/theme";
import AppRoutes from "./AppRoutes";
const App = () => (
  <ChakraProvider>
    <AppRoutes />
  </ChakraProvider>
);

export default App;
