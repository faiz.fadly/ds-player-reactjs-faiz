import { Center } from "@chakra-ui/react";
import { motion } from "framer-motion";
import sentuh from "../../../assets/logo.png";

const AppLoader = () => {
  return (
    <Center h="100vh">
      <motion.img
        src={sentuh}
        style={{
          width: 96,
        }}
        animate={{ scale: [1, 1.25, 1.25, 1, 1] }}
        transition={{ ease: "linear", duration: 2, repeat: Infinity }}
      />
    </Center>
  );
};

export default AppLoader;
