import { Suspense } from "react";
import { BrowserRouter, Navigate, Route, Routes, useNavigate } from "react-router-dom";
import { SWRConfig } from "swr";
import Preview from "../LayoutPreview/Preview";
import AppLoader from "./component/AppLoader";

const BaseRoutes = () => {
  const navigate = useNavigate();
  return (
    <SWRConfig
    //   value={{
    //     onError: (error) => {
    //       if (error.response.status === 500) {
    //         toast({
    //           position: "top",
    //           title: "500 Server Error",
    //           description: "Oops, something went wrong, navigate to dashboard, try again later.",
    //           status: "error",
    //           duration: 9000,
    //           isClosable: true,
    //         });
    //         navigate("/");
    //       }
    //     },
    //   }}
    >
      <Routes>
        <Route path="/" element={<Preview />}>
          <Route index element={<Preview />} />
          <Route path="/choose-template" element={<Preview />} />          
        </Route>
        <Route path="*" element={<Navigate to="/" />} />
      </Routes>
    </SWRConfig>
  );
};

const AppRoutes = () => {
  return (
    <Suspense fallback={<AppLoader />}>
      <BrowserRouter>
        <BaseRoutes />
      </BrowserRouter>
    </Suspense>
  );
};

export default AppRoutes;
