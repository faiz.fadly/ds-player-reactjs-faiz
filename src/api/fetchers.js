import axios from "axios";

export const fetcherPriview = async (url) => {
    const {
      data: { data },
    } = await axios.get(`${url}`);
    return data;
  };